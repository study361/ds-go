package main

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.POST("/data", DataHandler)
	router.GET("/ping", PingHandler)

	router.Run()
}

func DataHandler(c *gin.Context)  {
	time.Sleep(500 * time.Microsecond)
	c.JSON(http.StatusOK, gin.H{
		"data": "Success",
	})
}

func PingHandler(c *gin.Context)  {
	c.JSON(http.StatusOK, "Pong!")
}
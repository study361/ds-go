package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
)

type Response struct {
	Data string `json:"data"`
}

func SetupRouter() *gin.Engine {
	router := gin.Default()
	return router
}

func TestPingHandler(t *testing.T)  {
	var response string

	r := SetupRouter()
	r.GET("ping", PingHandler)

	req, _ := http.NewRequest("GET", "/ping", nil)
	w := httptest.NewRecorder()

	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	json.Unmarshal([]byte(w.Body.Bytes()), &response)

	assert.Equal(t, "Pong!", response)
}

func TestDataHandler(t *testing.T)  {
	var response Response

	r := SetupRouter()
	r.POST("data", DataHandler)

	req, _ := http.NewRequest("POST", "/data", nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
	json.Unmarshal([]byte(w.Body.Bytes()), &response)

	assert.Equal(t, "Success", response.Data)
}
